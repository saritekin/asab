class VideosController < ApplicationController
  require 'open-uri'
  require 'json'

  def index
    response = open('https://www.googleapis.com/youtube/v3/search?key=AIzaSyA1ViQVX1Q9dgNb8LBhynbeE17tLv07msc&channelId=UCtADbKzNiDJKRnCdddFxCIg&part=snippet,id&order=date&maxResults=50').read
    data_hash = JSON.parse(response)
    data_hash = data_hash['items']
    @videos = []
    data_hash.length.times { |i| @videos.push(data_hash[i]) }

  end

end
